package helloworld;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class HelloService {

    WebTarget baseUrl;

    public HelloService() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8081/");
    }

    public String hello() {
        return baseUrl.path("hello").request().get(String.class);
    }
}
