package helloworld;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class WorldService {

    WebTarget baseUrl;

    public WorldService() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8082/");
    }

    public String world() {
        return baseUrl.path("world").request().get(String.class);
    }
}
