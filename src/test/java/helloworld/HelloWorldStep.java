package helloworld;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;


public class HelloWorldStep {

    HelloService helloService;
    WorldService worldService;
    String response = "";
    
    @Given("I have two web servers")
    public void iHaveTwoWebServers() {
        helloService = new HelloService();
        worldService = new WorldService();
    }

    @When("I call the hello server")
    public void iCallTheHelloServer() {
        response = helloService.hello();
    }

    @And("I call the world server")
    public void iCallTheWorldServer() {
        response += " ";
        response += worldService.world();
    }

    @Then("I have a {string} response")
    public void iHaveAResponse(String arg0) {
        Assert.assertEquals("Hello World", response);
    }
}
